import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(private configService: ConfigService) {}

  getHello(): string {
    const port = this.configService.get<number>('PORT');
    const databasePassword = this.configService.get<string>('POSTGRES_PASSWORD');
    return `Hello World! Running on port ${port} password: ${databasePassword}`;
  }
}
