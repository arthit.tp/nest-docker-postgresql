
# NestJS Docker PostgreSQL Project

This project uses Docker Compose to run a NestJS application with PostgreSQL and pgAdmin. Below are the instructions to set up and run the project.

## Prerequisites

Make sure you have the following installed:

- Docker
- Docker Compose

## Project Structure

```
project-root/
│
├── Dockerfile
├── docker-compose.yml
├── .env
├── .env.development
├── .env.production
├── src/
│   ├── main.ts
│   ├── app.module.ts
│   └── ...
└── package.json
```

## Environment Variables

The project uses environment variables to configure the application. The main configuration file is `.env`, but you can also have environment-specific configurations with `.env.development` and `.env.production`.

### Example `.env` File

Create a `.env` file in the root directory with the following content:

```
POSTGRES_PASSWORD=postgres
PORT=3000
PGADMIN_DEFAULT_EMAIL=admin@admin.com
PGADMIN_DEFAULT_PASSWORD=pgadmin4

DB_HOST=db
DB_PORT=5432
DB_USERNAME=postgres
DB_PASSWORD=postgres
DB_DATABASE=postgres
```

### Development and Production Environment Files

You can create `.env.development` and `.env.production` files for different environments. The content will be similar to the `.env` file but with environment-specific values.

Example `.env.development`:

```
POSTGRES_PASSWORD=devpassword
PORT=3000
PGADMIN_DEFAULT_EMAIL=dev@admin.com
PGADMIN_DEFAULT_PASSWORD=devpassword

DB_HOST=db
DB_PORT=5432
DB_USERNAME=devuser
DB_PASSWORD=devpassword
DB_DATABASE=devdatabase
```

Example `.env.production`:

```
POSTGRES_PASSWORD=prodpasword
PORT=3000
PGADMIN_DEFAULT_EMAIL=prod@admin.com
PGADMIN_DEFAULT_PASSWORD=prodpassword

DB_HOST=db
DB_PORT=5432
DB_USERNAME=produser
DB_PASSWORD=prodpasword
DB_DATABASE=proddatabase
```

## Build and Run the Project

Follow these steps to build and run the project using Docker Compose.

1. **Clone the Repository**

   ```bash
   git clone https://gitlab.com/arthit.tp/nest-docker-postgresql.git
   cd nest-docker-postgresql
   ```

2. **Create Environment Files**

   Create a `.env` file as described above. If needed, also create `.env.development` and `.env.production` files.

3. **Build and Run the Containers**

   To build and run the containers, use the following command:

   ```bash
   docker-compose up --build
   ```

   This command will:

   - Build the Docker images
   - Start the PostgreSQL database
   - Start the NestJS application
   - Start pgAdmin

4. **Access the Application**

   - The NestJS application will be accessible at `http://localhost:3000`.
   - pgAdmin will be accessible at `http://localhost:5050`.

## Stopping the Containers

To stop the running containers, use the following command:

```bash
docker-compose down
```

## Additional Commands

- **View logs**:

  ```bash
  docker-compose logs app
  ```

- **Restart the application container**:

  ```bash
  docker-compose restart app
  ```

- **Rebuild the application container**:

  ```bash
  docker-compose up --build app
  ```

## Troubleshooting

If you encounter issues, check the logs for more information:

```bash
docker-compose logs app
```

For further assistance, you can check the Docker and Docker Compose documentation.

## Conclusion

This README provides the steps to set up and run the NestJS application with PostgreSQL and pgAdmin using Docker Compose. Adjust the environment variables as needed for your specific use case.
